import { useLocation } from "react-router-dom"
import "./style.css"

export const Home = () => {

    const {state} = useLocation()
    return(
        <div> 
            <h1>Home {state}</h1>
        </div>
    )
}