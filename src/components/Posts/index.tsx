import { Outlet, useParams, useSearchParams } from "react-router-dom"
import "./style.css"

export const Posts = () => {
    const {id} = useParams()
    const [Qs] = useSearchParams()

    return(
        <div> 
            <h1>Post {id&&`param-${id}`}  {Qs&&`Qs-${Qs}`}</h1>
            <Outlet/>
        </div>
    )
}